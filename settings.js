const settings = {
  server: {
    port: process.env.HTTP_PORT || "3000"
  },
  evtNetwork: {
    host: process.env.EVT_HOST || "testnet1.everitoken.io",
    port: process.env.EVT_PORT || 8888,
    protocol: process.env.EVT_PROTOCOL || "http"
  },
  passphrase: process.env.PRIVATE_KEY || "12345678901234567890",
  groupName: process.env.GROUP_NAME || "group3",
  domainName: process.env.DOMAIN_NAME || "domain"
};

module.exports = settings;
