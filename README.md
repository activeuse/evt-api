# evt-test

Setup: `npm install`

Server in watch mode (restarts after file changes): `npm watch`

Server: `npm start`

Initialization of group and domain: `node init.js`

It can be also useful to run node init.js to see current state of the domain and the group.
