const EvtService = require("./evt-service");
const settings = require("./settings");

const evtService = new EvtService(settings);

(async function() {
  try {
    await init();
  } catch (e) {
    console.error(e);
  }
})();

async function init() {
  const group = await initGroup();
  const doman = await initDomain();
}

async function initGroup() {
  let group;
  try {
    group = await evtService.getGroup(settings.groupName);
  }
  catch (e) {
    // do nothing
  }

  if (!group) {
    await evtService.createGroup(settings.groupName);
    group = await evtService.getGroup(settings.groupName);
    console.log(`Group ${settings.groupName} has been created`, group);
  } else {
    console.log(`Group ${settings.groupName} already exists`, JSON.stringify(group, null, 2));
  }

  return group;
}

async function initDomain() {
  let domain;
  try {
    domain = await evtService.getDomain(settings.domainName);
  }
  catch (e) {
    // do nothing
  }

  if (!domain) {
    await evtService.createDomain(settings.domainName);
    domain = await evtService.getDomain(settings.domainName);
    console.log(`Domain ${settings.domainName} has been created`, domain);
  } else {
    console.log(`Domain ${settings.domainName} already exists`, JSON.stringify(domain, null, 2));
  }

  return domain;
}