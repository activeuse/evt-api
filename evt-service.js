const EVT = require("evtjs");

class EvtService {
  constructor(settings) {
    this.settings = settings;
    this.evt = EVT({
      endpoint: settings.evtNetwork,
      keyProvider: EVT.EvtKey.seedPrivateKey(settings.passphrase)
    });
  }

  getPublicKey(keyphrase) {
    if (typeof keyphrase !== "string" || keyphrase.length < 20) {
      throw new Error("Non-valid keyphrase");
    }

    const privateKey = EVT.EvtKey.seedPrivateKey(keyphrase);
    const publicKey = EVT.EvtKey.privateToPublic(privateKey);

    return publicKey;
  }

 getPrivateKey(keyphrase) {
   if (typeof keyphrase !== "string" || keyphrase.length < 20) {
      throw new Error("Non-valid keyphrase");
    }

    return EVT.EvtKey.seedPrivateKey(keyphrase);
  }

  async createWallet(keyphrase) {
    await this.addUserToDomain(
      this.settings.domainName,
      this.getPublicKey(keyphrase)
    );
  }

  createDomain(domainName) {
    return this.evt.pushTransaction(
      actions.createDomain(
        domainName,
        this.getPublicKey(this.settings.passphrase)
      )
    );
  }

  transferTokens(keyphrase, pubk1, pubk2, ntokens, msg) {
   if (typeof keyphrase !== "string" || keyphrase.length < 20) {
      throw new Error("Non-valid keyphrase");
    }
    this.evt = EVT({
      endpoint: this.settings.evtNetwork,
      keyProvider: EVT.EvtKey.seedPrivateKey(keyphrase)
    });
    return this.evt.pushTransaction(
      {maxCharge: 120},
      actions.transferTokens(
        pubk1,
        pubk2,
        ntokens,
        msg
      )
    );
  }


  async addUserToDomain(domainName, userPublicKey) {
    const domain = await this.getDomain(domainName);
    const authorizers = domain.issue.authorizers;

    const authorizerRef = "[A] " + userPublicKey;

    if (!authorizers.some(x => x.ref === authorizerRef)) {
      authorizers.push({
        ref: authorizerRef,
        weight: 1
      });
    }

    return this.evt.pushTransaction(
      actions.updateDomain(
        domainName,
        domain.issue,
        domain.transfer,
        domain.manage
      )
    );
  }

  getDomain(domainName) {
    return this.evt.getDomainDetail(domainName);
  }

  createGroup(groupName) {
    return this.evt.pushTransaction(
      actions.createGroup(
        groupName,
        this.getPublicKey(this.settings.passphrase),
        this.getPublicKey(this.settings.passphrase + "/owner")
      )
    );
  }

  createNewSuspendedTransaction(domainName, proposalName, proposer) {
    return this.evt.pushTransaction(
      actions.createSuspendedTransaction(domainName, proposalName, proposer)
    );
  }

  getGroup(groupName) {
    return this.evt.getGroupDetail(groupName);
  }

  getEstimatedCharge(action) {
    return this.evt.getEstimatedChargeForTransaction(action);
  }

  async getFungableBalance(publicKey) {
    if (!EVT.EvtKey.isValidPublicKey(publicKey)) {
      throw new Error("Public key is not valid");
    }
     const res = await this.evt.getFungibleBalance(publicKey);
     return res;
  }

  async getTransactionsDetailOfPublicKeys(publicKeys){
   return await this.evt.getTransactionsDetailOfPublicKeys(publicKeys);
  }
}

const actions = {
  createGroup(groupName, publicKey, ownerPublicKey) {
    return new EVT.EvtAction("newgroup", {
      name: groupName,
      group: {
        name: groupName,
        key: publicKey,
        root: {
          threshold: 2,
          nodes: [
            {
              key: ownerPublicKey,
              weight: 3
            }
          ]
        }
      }
    });
  },

  transferTokens(pubk1, pubk2, ntokens, msg){
   return new EVT.EvtAction("transferft", {
	from: pubk1,
	to: pubk2,
	number: ntokens,
	memo: msg})
  },

  createDomain(domainName, publicKey) {
    return new EVT.EvtAction("newdomain", {
      name: domainName,
      creator: publicKey,
      issue: {
        name: "issue",
        threshold: 1,
        authorizers: [
          {
            ref: "[A] " + publicKey,
            weight: 1
          }
        ]
      },
      transfer: {
        name: "transfer",
        threshold: 1,
        authorizers: [
          {
            ref: "[G] .OWNER",
            weight: 1
          }
        ]
      },
      manage: {
        name: "manage",
        threshold: 1,
        authorizers: [
          {
            ref: "[A] " + publicKey,
            weight: 1
          }
        ]
      }
    });
  },

  updateDomain(domainName, issue, transfer, manage) {
    return new EVT.EvtAction("updatedomain", {
      name: domainName,
      issue: issue,
      transfer: transfer,
      manage: manage
    });
  },

  createSuspendedTransaction(domainName, proposalName, proposer) {
    return new EVT.EvtAction("newsuspend", {
      name: domainName,
      trx: proposalName,
      proposer: proposer
    });
  }
};

module.exports = EvtService;
