const express = require("express");
const app = express();
const router = require("express-promise-router")();

const EvtService = require("./evt-service");
const settings = require('./settings');

const evtService = new EvtService(settings);

// declare: get wallet public key
router.get("/wallet/name", async (req, res) => {
  const passphrase = req.query.passphrase || "";
  res.json({ name: evtService.getPublicKey(passphrase) });
});

// declare: create a new wallet
router.post("/wallet", async (req, res) => {
  const passphrase = req.body.passphrase;
  await evtService.createWallet(passphrase);
  res.end();
});


// declare: make transaction from pk1 to pk2
router.post("/wallet/sendto", async (req, res) => {
  const keyphrase = req.body.keyphrase;
  const pubkey1 = req.body.pubkey1;
  const pubkey2 = req.body.pubkey2;
  const transsum = req.body.transsum;
  const msg = req.body.msg;
  await evtService.transferTokens(keyphrase, pubkey1, pubkey2, transsum, msg);
  res.end();
});


// declare: get transactions of user
router.get("/wallet/:key/transactions", async (req, res) => {
  const key = req.params.key;
  const trans = await evtService.getTransactionsDetailOfPublicKeys([key]);
  res.json({ transactions: trans });
});


// declare: get fungable tokens of existing wallet
router.get("/wallet/:key/balance", async (req, res) => {
  const key = req.params.key;
  const tokens = await evtService.getFungableBalance(key);
  console.log(tokens)
  res.json({ tokens: tokens });
});

// parse body as json
app.use(express.json());

// add routes
app.use(router);

// error handler
app.use(function(err, req, res, next) {
  console.error(
    `Error occurred on processing: ${req.path}\n` +
      `-----------\n${err.stack}\n---------`
  );
  res.status(500);
  res.json({ error: err.message || "Internal error occurred" });
});

app.listen(settings.server.port, () => {
  console.log(`Server listening on port ${settings.server.port}`);
});
